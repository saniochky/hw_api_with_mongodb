const {Schema, model} = require('mongoose');

const schema = new Schema({
  username: {
    type: String,
    required: true,
  },
  encryptedPassword: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    default: new Date(Date.now()).toISOString(),
  },
});

module.exports = model('user', schema);
