const {Schema, model} = require('mongoose');

const schema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    default: new Date(Date.now()).toISOString(),
  },
});

module.exports = model('note', schema);
