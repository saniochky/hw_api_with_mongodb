const Joi = require('joi');

const schema = Joi.object({
  username: Joi.string()
      .alphanum()
      .min(6)
      .max(20)
      .required(),

  password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{6,20}$'))
      .required(),
});

module.exports = schema;
