const Joi = require('joi');

const schema = Joi.object({
  text: Joi.string()
      .required(),
});

module.exports = schema;
