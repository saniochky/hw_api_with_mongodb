const bcrypt = require('bcrypt');
const config = require('config');
const express = require('express');
const jwt = require('jsonwebtoken');
const loginValidation = require('../validation/login_validation');
const userSchema = require('../models/user');

const saltRounds = 10;
const secretKey = config.get('Customer.dbConfig.secretKey');
const router = express.Router();

router.post('/register', checkBody, register, (req, res) => {
  res.json({
    'message': 'Success',
  });
});

router.post('/login', checkBody, login, (req, res) => {
  res.json({
    'message': 'Success',
    'jwt_token': jwt.sign({
      _id: req.userId,
      encryptedPassword: req.encryptedPassword,
    }, secretKey),
  });
});

function checkBody(req, res, next) {
  const body = loginValidation.validate(req.body);

  if (body.error) {
    res.status(400).json({
      'message': body.error.message,
    });
  } else {
    next();
  }
}

async function register(req, res, next) {
  try {
    const existingUser = await userSchema.findOne({username: req.body.username});

    if (existingUser) {
      res.status(400).json({
        'message': 'User with such username already exists',
      });
    } else {
      const user = new userSchema({
        username: req.body.username,
        encryptedPassword: bcrypt.hashSync(req.body.password, saltRounds),
      });

      await user.save();
      next();
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).json({
      'message': 'Internal server error',
    });
  }
}

async function login(req, res, next) {
  try {
    const user = await userSchema.findOne({username: req.body.username});

    if (!user) {
      res.status(400).json({
        'message': 'User with such username doesn\'t exist',
      });
    } else if (!bcrypt.compareSync(req.body.password, user.encryptedPassword)) {
      res.status(400).json({
        'message': 'Password is incorrect',
      });
    } else {
      req.userId = user._id;
      req.encryptedPassword = user.encryptedPassword;
      next();
    }
  } catch (error) {
    res.status(500).json({
      'message': 'Internal server error',
    });
  }
}

module.exports = router;
