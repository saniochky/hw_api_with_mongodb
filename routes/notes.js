const express = require('express');
const noteSchema = require('../models/note');
const noteValidation = require('../validation/note_validation');
const validateJWT = require('../validation/jwt_validation');

const router = express.Router();

router.route('/')
    .get(validateJWT, getQueryParams, getNotes, (req, res) => {
      res.json({
        'offset': req.offset,
        'limit': req.limit,
        'count': req.notes.length,
        'notes': req.notes.map((note) => {
          return {
            '_id': note._id,
            'userId': note.userId,
            'completed': note.completed,
            'text': note.text,
            'createdDate': note.createdDate,
          };
        }),
      });
    })
    .post(validateJWT, checkBody, postNote, (req, res) => {
      res.json({
        'message': 'Success',
      });
    });

router.route('/:id')
    .get(validateJWT, getOneNote, (req, res) => {
      res.json({
        '_id': req.note._id,
        'userId': req.note.userId,
        'completed': req.note.completed,
        'text': req.note.text,
        'createdDate': req.note.createdDate,
      });
    })
    .put(validateJWT, checkBody, updateNote, (req, res) => {
      res.json({
        'message': 'Success',
      });
    })
    .patch(validateJWT, checkUncheckNote, (req, res) => {
      res.json({
        'message': 'Success',
      });
    })
    .delete(validateJWT, deleteNote, (req, res) => {
      res.json({
        'message': 'Success',
     });
    });

function getQueryParams(req, res, next) {
  const offset = req.query.offset;
  const limit = req.query.limit;

  req.offset = (offset && /^(0|([1-9]\d*))$/.test(offset)) ? +offset : 0;
  req.limit = (limit && /^(0|([1-9]\d*))$/.test(limit)) ? +limit : 0;

  next();
}

async function getNotes(req, res, next) {
  try {
    let notes = await noteSchema.find({
      userId: req.userId,
    });

    if (req.offset) {
      notes = notes.slice(-(notes.length - req.offset));
    }
    if (req.limit) {
      notes = notes.slice(0, req.limit);
    }

    req.notes = notes;
    next();
  } catch (error) {
    res.status(500).json({
      'message': 'Internal server error',
    });
  }
}

function checkBody(req, res, next) {
  const body = noteValidation.validate(req.body);

  if (body.error) {
    res.status(400).json({
      'message': body.error.message,
    });
  } else {
    next();
  }
}

async function postNote(req, res, next) {
  try {
    const note = new noteSchema({
      userId: req.userId,
      text: req.body.text,
    });

    await note.save();
    next();
  } catch (error) {
    res.status(500).json({
      'message': 'Internal server error',
    });
  }
}

async function getOneNote(req, res, next) {
  try {
    const note = await noteSchema.findById(req.params.id);

    if (note) {
      req.note = note;
      next();
    } else {
      res.status(400).json({
        'message': 'Note id is invalid',
      });
    }
  } catch (error) {
    res.status(500).json({
      'message': 'Internal server error',
    });
  }
}

async function updateNote(req, res, next) {
  try {
    const note = await noteSchema.findById(req.params.id);

    if (note) {
      if (note.userId === req.userId) {
        note.text = req.body.text;
        await note.save();
        next();
      } else {
        res.status(400).json({
          'message': 'You cannot edit this note',
        });
      }
    } else {
      res.status(400).json({
        'message': 'Note id is invalid',
      });
    }
  } catch (error) {
    res.status(500).json({
      'message': 'Internal server error',
    });
  }
}

async function checkUncheckNote(req, res, next) {
  try {
    const note = await noteSchema.findById(req.params.id);

    if (note) {
      if (note.userId === req.userId) {
        note.completed = !note.completed;
        await note.save();
        next();
      } else {
        res.status(400).json({
          'message': 'You cannot check/uncheck this note',
        });
      }
    } else {
      res.status(400).json({
        'message': 'Note id is invalid',
      });
    }
  } catch (error) {
    res.status(500).json({
      'message': 'Internal server error',
    });
  }
}

async function deleteNote(req, res, next) {
  try {
    const note = await noteSchema.findById(req.params.id);

    if (note) {
      if (note.userId === req.userId) {
        noteSchema.deleteOne({'_id': req.params.id}).then(() => {
          next();
        }).catch((e) => {
          res.status(400).json({
            'message': 'Note id is invalid',
          });
        });
      } else {
        res.status(400).json({
          'message': 'You cannot edit this note',
        });
      }
    }
  } catch (error) {
    res.status(500).json({
      'message': 'Internal server error',
    });
  }
}

module.exports = router;
